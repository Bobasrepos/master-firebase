import React from 'react';

let html =  (<table width="100%" border="0" cellSpacing="2" cellPadding="0"
                       style="font-family: arial, sans-serif; font-size: 10pt; color: black;">
                <tr>
                    <td colSpan="2" valign="top"><strong>Contact Information</strong></td>
                </tr>
                <tr>
                    <td width="150" valign="top">Follow-up instructions:</td>
                    <td> It's best to call around 4:30pm EST.</td>
                </tr>
                <tr>
                    <td width="150" valign="top">Company name:</td>
                    <td>Re/max</td>
                </tr>
                <tr>
                    <td width="150" valign="top">Contact name:</td>
                    <td>Mohamed Nada</td>
                </tr>
                <tr>
                    <td width="150" valign="top">Job title:</td>
                    <td>Realtor</td>
                </tr>
                <tr>
                    <td width="150" valign="top">Office:</td>
                    <td>(416) 832-3779</td>
                </tr>
                <tr>
                    <td width="150" valign="top">Email:</td>
                    <td><a href="mailto:mohamednnada@hotmail.com">mohamednnada@hotmail.com</a></td>
                </tr>
                <tr>
                    <td width="150" valign="top">Address:</td>
                    <td><i>Unspecified</i></td>
                </tr>
                <tr>
                    <td width="150" valign="top">City:</td>
                    <td>Oakville</td>
                </tr>
                <tr>
                    <td width="150" valign="top">State:</td>
                    <td>ON</td>
                </tr>
                <tr>
                    <td width="150" valign="top">ZIP code:</td>
                    <td>L5C 4E9</td>
                </tr>
                <tr>
                    <td width="150" valign="top">Country:</td>
                    <td>Canada</td>
                </tr>
                <tr>
                    <td width="150" valign="top">Timestamp:</td>
                    <td>1/7/19 12:26 CST</td>
                </tr>
                <tr>
                    <td colSpan="2">&nbsp;</td>
                </tr>
                <tr>
                    <td colSpan="2" valign="top"><strong>Company Profile</strong></td>
                </tr>
                <tr>
                    <td width="150" valign="top">Segment:</td>
                    <td>Real Estate - B2C. This is a residential real estate team that needs a CRM system.</td>
                </tr>
                <tr>
                    <td width="150" valign="top">Size (Annual Revenue):</td>
                    <td>Less than $1 million</td>
                </tr>
                <tr>
                    <td width="150" valign="top">Number of Employees:</td>
                    <td>2 to 5 employees. 2.</td>
                </tr>
                <tr>
                    <td width="150" valign="top">Number of Users:</td>
                    <td>2 to 5 users. 2.</td>
                </tr>
                <tr>
                    <td colSpan="2">&nbsp;</td>
                </tr>
                <tr>
                    <td colSpan="2" valign="top"><strong>Project &amp; Requirements Overview</strong></td>
                </tr>
                <tr>
                    <td width="150" valign="top">Applications needed:</td>
                    <td>Sales Automation</td>
                </tr>
                <tr>
                    <td width="150" valign="top">Key features needed:</td>
                    <td>They need a system to house a contact database, make interaction notes, schedule follow-ups and
                        reminders, and track relationships with prospects and clients over time. Email automation would be
                        nice but isn't strictly necessary.
                    </td>
                </tr>
                <tr>
                    <td width="150" valign="top">Deployment:</td>
                    <td>Web-based</td>
                </tr>
                <tr>
                    <td width="150" valign="top">Currently using:</td>
                    <td> Excel, Constant, Gmail.</td>
                </tr>
                <tr>
                    <td width="150" valign="top">Reasons for shopping:</td>
                    <td>It's tedious and labor-intensive to manage all of this by hand.</td>
                </tr>
                <tr>
                    <td width="150" valign="top">Who they've evaluated:</td>
                    <td>WizeAgent.</td>
                </tr>
                <tr>
                    <td width="150" valign="top">Price expectations:</td>
                    <td>Screened for $50/user/month plus $300 in setup fees.</td>
                </tr>
                <tr>
                    <td width="150" valign="top">Timeframe:</td>
                    <td>Immediate</td>
                </tr>
                <tr>
                    <td colSpan="2">&nbsp;</td>
                </tr>
                <tr>
                    <td colSpan="2" valign="top"><strong>Next Steps</strong></td>
                </tr>
                <tr>
                    <td width="150" valign="top">Request:</td>
                    <td>Demo</td>
                </tr>
                <tr>
                    <td width="150" valign="top">Product:</td>
                    <td>FreeAgent CRM</td>
                </tr>
                <tr>
                    <td width="150" valign="top">Contact notes:</td>
                    <td>Mohammed was very friendly and professional on the phone. His father is the broker and asked him to
                        do this initial research; they will make a final decision together.
                    </td>
                </tr>
                <tr>
                    <td width="150" valign="top">Qualified By:</td>
                    <td>Allen</td>
                </tr>
            </table>
        );

let htmlNode = () => {
    let dataArray = html.props.children;
    console.log(html.props.children);
    let fiealds = dataArray.map(data => console.log(data.props.children[0] !== undefined ? data.props.children[0].props.children + " " + data.props.children[1].props.children : data.props.children.props ));
    return ;
}

let EmailParser = () => {
    let htmlCode = htmlNode();
    console.log(htmlCode);
  return <div>test</div>
};

export { EmailParser };